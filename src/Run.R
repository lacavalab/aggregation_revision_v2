# David Coit (based on earlier version by Mehrnoosh Oghbaie)
## HEK293T DMSO/ATM inhibition
## U2OS DMSO/CPT treated
## 10/28/2021

## Define class and run the project from this file
rm(list=ls())

#renv::init(bare = TRUE)
renv::restore()
if (!requireNamespace("rstudioapi", quietly = TRUE))
  install.packages("rstudioapi")

setwd(
  paste0(dirname(rstudioapi::getActiveDocumentContext()$path))
)

set.seed(123)
CRAN.packages <- c("rstatix","readr","readxl", "xlsx","data.table","reshape2","dplyr","magrittr","fpc","cluster","dendextend","gplots","renv",
                   "igraph","sqldf","stringr","corrplot","ggplot2","R6","ggridges","progress","heatmaply","phylogram","ape","doParallel",
                   "gridExtra","ggrepel","rgl","venn", "writexl","outliers","ggforce", "gtable","factoextra","foreach",'BayesFactor')
bioconductor.packages <- c("PTXQC","org.Hs.eg.db","clusterProfiler","ReactomePA", "AnnotationHub","DOSE")

source("functions/Functions.R")
install.packages.if.necessary(CRAN.packages,bioconductor.packages)
source("functions/Anova_Analysis.R")
input.dir <- "../input/txt"
# runQC(input.dir)

Comparison <- rbind(
  cbind("HEK293", "ATM", "pellet"),
  cbind("HEK293", "DMSO", "pellet"),
  cbind("HEK293", "ATM", "WCL"),
  cbind("HEK293", "DMSO", "WCL"),
  cbind("U2OS", "CPT", "pellet"),
  cbind("U2OS", "DMSO", "pellet"),
  cbind("U2OS", "CPT", "WCL"),
  cbind("U2OS", "DMSO", "WCL")
)


MSInput <- TemplateProtein$new()$
  importInput(input.dir)$
  removeContaminant()$
  transformData()$
  choosingConditions(Comparison)$
  anovaAnalysis(Comparison)$
  visualize(Comparison)$
  enrichment()$
  drawScatterplot()$
  writeFiles()

save(MSInput, file='backUp.RData')
#load('backUp.RData')

#renv::snapshot()
